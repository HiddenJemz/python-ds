# Data Science Statistics using Python: describe()

import pandas

Marvel_Profit = pandas.read_csv("MarvelData.csv", header=0, sep=",")

pandas.set_option('display.max_columns', None)
pandas.set_option('display.max_rows', None)

print(Marvel_Profit.describe())
# ---------------------------------------------------------------------

# Percentile: 

import numpy

Max_Gross = Marvel_Profit["Lifetime_Gross in Millions"]

tenth_percentile = numpy.percentile(Max_Gross, 10)

print('\n 10% percentile = ', tenth_percentile)
# ---------------------------------------------------------------------


# Standard Deviation: std()

# import pandas
# import numpy

# Marvel_Profit = pandas.read_csv("MarvelData.csv", header=0, sep=",")

stanDevi = numpy.std(Marvel_Profit)

print('\nStandard Deviation\n', stanDevi)
# ---------------------------------------------------------------------

# Coefficient of Variation

# import numpy

# Marvel_Profit = pandas.read_csv("MarvelData.csv", header=0, sep=",")

coefVari = numpy.std(Marvel_Profit) / numpy.mean(Marvel_Profit)

print('\nCoefficient of Variation\n', coefVari)
# ---------------------------------------------------------------------

# Stat Variance



