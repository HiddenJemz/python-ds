### Data Science Statistics

**describe()** Used to summerize data.

```python

import pandas

Marvel_Profit_Data = pandas.read_csv("MarvelData.csv", header=0, sep=",")

pandas.set_option('display.max_columns', None)
pandas.set_option('dispaly.max_row', None)

print(Marvel_Profit_Data.describe())

```
**std()** Function of Numpy to find the Standard Deviation of a variable. 
Use the same Numpy library to calculate the coefficient of variation.


