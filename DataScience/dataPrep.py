import pandas

Marvel_data = pandas.read_csv("MarvelData.csv", header=0, sep=",")

print(Marvel_data)


# If the csv data is large, use head() function to show the top 5
# Example: print(Marveldata.head())
#print(Marvel_data.head())

# To remove blank rows, use dropna() function
#Example: Marvel_data.dropna()

# To retrieve the Data Type, use the info() function
#Example: print(Marvel_data.info())

# Its a MUST to convert the object type to a decimal float64 to perform data analysis.
# Use the astype() function to convert data to float64
#Example: Marvel_data["Lifetime Gross"] = Marvel_data["Lifetime Gross"].astype(float)
#Example: Marvel_data["Rank"] = Marvel_data["Rank"].astype(float)

# Finally: To Analyze the Data, use the describe() function 
print(Marvel_data.describe())

