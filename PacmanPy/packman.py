# Creating the classic Pacman by following youtuber: https://www.youtube.com/watch?v=9H27CimgPsQ&t=6s

from board import boards
import pygame

pygame.init()

# Board Size
WIDTH = 900
HEIGHT = 950
screen = pygame.display.set_mode([WIDTH, HEIGHT])

# Game Speed/Pace
timer = pygame.time.Clock()
fps = 60

# Game Font
font = pygame.font.Font('freesansbold.ttf', 20)

# Adding the 1st board level.
level =  boards

# Function to call the board using // floor division
def draw_board(lvl):
    num1 = ((HEIGHT - 50)//32)
    num2 = (WIDTH//32)

    # Nested for loop to go build the level
    for i in range(len(lvl)):
        for j in range(len(lvl[i])):
            if lvl[i][j] == 1:
                pygame.draw.circle(screen, 'white', (j * num2 + (0.5 * num2), i * num1 + (0.5 * num1)), 4)

            if lvl[i][j] == 2:
                pygame.draw.circle(screen, 'white', (j * num2 + (0.5 * num2), i * num1 + (0.5 * num1)), 10)

            if lvl[i][j] == 3:
                pygame.draw.line(screen, 'blue', (j * num2 + (0.5 * num2), i * num1), (j * num2 + (0.5 * num2), i * num1 + num1), 3)
            if lvl[i][j] == 4:
                pygame.draw.line(screen, 'blue', (j * num2 + (0.5 * num2), i * num1), (j * num2 + (0.5 * num2), i * num1 + num1), 3)
            if lvl[i][j] == 5:
                pygame.draw.line(screen, 'blue', (j * num2 + (0.5 * num2), i * num1), (j * num2 + (0.5 * num2), i * num1 + num1), 3)
# Game Play
run = True
while run:
    timer.tick(fps)
    screen.fill('black')

    # To visually see the board level
    draw_board(level)

    # Ending the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    pygame.display.flip()
pygame.quit()

# Dont forget to start back @ 23:24


