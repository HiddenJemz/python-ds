class Book:
    def __init__(self, title, author):
        self.title = title
        self.author = author

    def __str__(self):
        return 'Book: {} by {}'.format(self.title, self.author)

    class TextBook:
        def __init__(self, title, author, edition):
            self.author = author
            self.title = title
            self.edition = edition

book = Book(title='All of the lights', author='Kanye and Rihanna')
print(book)

