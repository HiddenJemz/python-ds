

class A:
    def say_my_name(self):
        print('A')

class B(A):
    def say_my_name(self):
        print('B')

class C(A):
    def say_my_name(self):
        print('C')

class D(B,C):
    def say_my_name(self):
        pass

a = A()
b = B()
c = C()
d = D()

# Order matters with the class D and will display the class B first.
a.say_my_name()
b.say_my_name()
c.say_my_name()
d.say_my_name()
            
