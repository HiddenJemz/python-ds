import random
from actors import Player, Enemy

def main():
    print_intro()
    play()

def print_intro():
    print('''
        Monster Slash *!!*

        Ready Player One:
        {Press [Enter] To Start}
    ''')
    input()

def play():
    enemies = [
        Enemy('Ogre', 1),
        Enemy('Imp', 1)
    ]
    player = Player('Hercules', 1)
    while True:
        next_enemy = random.choice(enemies)
        cmd = input('You see an {}. [r]un, [a]ttack, [p]ass: '.format(next_enemy.kind))
        if cmd == 'r':
            print('{} Run Away!'.format(player.name))
        elif cmd == 'a':
            print('{} Attack!! {}'.format(player.name, next_enemy.kind))
            if player.attacks(next_enemy):
                enemies.remove(next_enemy)
            else:
                print('{} hide to plan your next move'.format(player.name))
        elif cmd == 'p':
            print('Still thinking of your next move...')
        else:
            print('Choose a valid option!')

        print()
        print('*'*30)
        print()

        if not enemies:
            print('{} have won! Congratulations!!'.format(player.name))
            break

if __name__ == '__main__':
    main()
    
