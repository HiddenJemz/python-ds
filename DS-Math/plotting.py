# The plot() function is used to make 2D hexagonal binning plots of points x:y

import matplotlib.pyplot as plt
import pandas

# Create a variable to use for plotting
marvelData = pandas.read_csv('MarvelData.csv')

# Defining the x & y axis date to use from the .csv. plot(file[x-axis], file[y-axis]
plt.plot(marvelData['Rank'], marvelData['Lifetime_Gross in Millions'])

# Labeling the chart
plt.xlabel('Rank')
plt.ylabel('Profit Gross')
plt.title('Marvel Profit Data')

plt.show()

        
