# Linear Functions - Equations used to relate one variable to another.
***Has one independant variable (x) and an dependant varible (y).***
```
y = ax + b

OR

f(x) = ax + b
```
**Linear f(x) with one explanatory variable.**
```
f(x)  = 4x + 20
```
**Plotting a Linear Function**
![Plot](LinearPlot.png)

**Graph Explained**\
X-Axis = Average_Pulse line (Horizontal line)\
Y-Axis = Calorie_Burnage (Vertical line)\
Blue-Line = The structure of the mathematical f(x) of prediction.
---
## Results from the python script plotting.py

![plotting.py](Python-Plotting.png)
---
## Demonstrating the Slope Intercept

![slopes.py](SlopeIntercept.png)
---

