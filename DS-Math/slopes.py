# Using Python to find the slope

# def slope(x1,y1,x2,y2):
#     slp = (y2 - y1)/(x2 - x1)
#     return slp
# 
# print(slope(80,240,90,260))

# ------------------------------------
# The Slope Intercept Formula

#import pandas
#import numpy
#
# Create a variable to use to determine the slope
#marvelData = pandas.read_csv('MarvelData.csv', header = 0, sep = ',')

# x, y variables to use with the numpy.polyfit library
#x = marvelData['Rank']
#y = marvelData['Lifetime_Gross in Millions']
#slope_intercept = numpy.polyfit(x,y,1)
#
#print(slope_intercept)

# ------------------------------------
import pandas
import matplotlib.pyplot as plt

# Create a variable to use for plotting
marvelData = pandas.read_csv('MarvelData.csv')

# Defining the x & y axis date to use from the .csv. plot(file[x-axis], file[y-axis]
marvelData.plot('Rank', 'Lifetime_Gross in Millions', kind='line')
# The x, y plot limits
plt.ylim(ymin = 0, ymax = 900)
plt.xlim(xmin = 0, xmax = 40)

# Labeling the chart
plt.ylabel('Rank')
plt.xlabel('Gross Profits')
plt.title('Marvel Slope Data')

plt.show()
# ------------------------------------
